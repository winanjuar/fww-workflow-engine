import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  Client,
  logger,
  Variables,
  ClientConfig,
} from 'camunda-external-task-client-js';
import * as camunda from 'camunda-external-task-client-js';
import { MemberService } from './member.service';
import { IWorkflowDetail } from 'src/interface/workflow.interface';
import { ClientProxy } from '@nestjs/microservices';
import { EPatternMessage } from 'src/core/pattern-message.enum';
import { RegulationService } from './regulation.service';
import { IDisdukcapilRequest } from 'src/interface/disdukcapil-request.interface';
import { IImigrasiRequest } from 'src/interface/imigrasi-request.interface';
import { IPeduliLindungiRequest } from 'src/interface/pedulilindungi-request.interface';
import { IMailNormal } from 'src/interface/mail-normal.interface';
import { IMailDecline } from 'src/interface/mail-decline.interface';
import { RuleService } from './rule.service';
import { IDestinationRequest } from 'src/interface/destination-request.interface';
import {
  calculateDeadlineBooking,
  calculateDeadlineRedeem,
} from 'src/utils/time-formatter';
import { EReservationStatus } from 'src/core/reservation-status.enum';
import { IFastPaymentRequest } from 'src/interface/fast-payment-request.interface';
import { BookingService } from './booking.service';
import { IReservationUpdateRequest } from 'src/interface/reservation-update.interface';
import { ISeatStatusUpdate } from 'src/interface/seat-search.interface';
import { ESeatStatus } from 'src/core/seat-status.enum';

@Injectable()
export class AppService {
  constructor(
    @Inject('CoreEngine') private readonly coreClient: ClientProxy,
    @Inject('BookingEngine') private readonly bookingClient: ClientProxy,
    private readonly configService: ConfigService,
    private readonly memberService: MemberService,
    private readonly bookingService: BookingService,
    private readonly regulationService: RegulationService,
    private readonly ruleService: RuleService,
  ) {}

  private readonly logger = new Logger(AppService.name);
  private readonly config: ClientConfig = {
    baseUrl: `http://${this.configService.get<string>(
      'BPM_HOST',
    )}:${this.configService.get<string>('BPM_PORT')}/engine-rest`,
    use: logger,
    maxTasks: 4,
  };
  private readonly client = new Client(this.config);

  async subscribeWorkers() {
    this.client.subscribe(
      'CheckBlacklistMember',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleCheckBlacklistMember.bind(this),
    );

    this.client.subscribe(
      'CheckDisdukcapil',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleCheckDisdukcapil.bind(this),
    );

    this.client.subscribe(
      'CheckImigrasi',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleCheckImigration.bind(this),
    );

    this.client.subscribe(
      'CheckPeduliLindungi',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleCheckPeduliLindungi.bind(this),
    );

    this.client.subscribe(
      'CheckPayment',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleCheckPayment.bind(this),
    );

    this.client.subscribe(
      'CalculateDiscountDestination',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleCalculateDiscountDestination.bind(this),
    );

    this.client.subscribe(
      'CalculateDiscountFastPayment',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleCalculateDiscountFastPayment.bind(this),
    );

    this.client.subscribe(
      'SendMailBookingCode',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleSendMailBookingCode.bind(this),
    );

    this.client.subscribe(
      'SendMailReservationCode',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleSendMailReservationCode.bind(this),
    );

    this.client.subscribe(
      'SendMailNotPassedReservation',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleSendMailNotPassedReservation.bind(this),
    );

    this.client.subscribe(
      'SendMailUnpaidReservation',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleSendMailUnpaidReservation.bind(this),
    );

    this.client.subscribe(
      'SendMailUnsuccessfulPayment',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleSendMailUnsuccessfulPayment.bind(this),
    );

    this.client.subscribe(
      'SendMailUnsuccessfulRedeemTicket',
      { processDefinitionKey: this.configService.get<string>('BPM_KEY') },
      this.handleSendMailUnsuccessfulRedeemTicket.bind(this),
    );
  }

  reportWorkflow(task: camunda.Task) {
    const reportWorkflow: IWorkflowDetail = {
      workflow: { processInstanceId: task.processInstanceId },
      taskId: task.id,
      activityName: task.activityId,
    };
    this.bookingClient.emit(
      EPatternMessage.REPORT_TASK_WORKFLOW,
      reportWorkflow,
    );
    this.logger.log(`Emit report ${task.id} done`);
  }

  updateReservation(dataReservation: IReservationUpdateRequest) {
    this.coreClient.emit(EPatternMessage.UPDATE_RESERVATION, dataReservation);
    this.logger.log(`Emit data update reservation done`);
  }

  sendMailNormal(dataMail: IMailNormal) {
    this.bookingClient.emit(EPatternMessage.SEND_NORMAL_MAIL, dataMail);
    this.logger.log(`Emit data mail normal done`);
  }

  sendMailDecline(dataMail: IMailDecline) {
    this.bookingClient.emit(EPatternMessage.SEND_DECLINE_MAIL, dataMail);
    this.logger.log(`Emit data mail decline done`);
  }

  async handleCheckBlacklistMember({ task, taskService }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const memberId = task.variables.get('memberId');

      const status = await this.memberService.getStatusMember(memberId);
      if (status.isBlacklist) {
        processVariables.set('statusMember', 'Not OK');
      } else {
        processVariables.set('statusMember', 'OK');
      }

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleCheckDisdukcapil({ task, taskService }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const check: IDisdukcapilRequest = {
        identityNumber: task.variables.get('identityNumber'),
        birthDate: task.variables.get('birthDate'),
        name: task.variables.get('name'),
      };

      const status = await this.regulationService.checkDisdukcapil(check);
      if (status.match) {
        processVariables.set('statusDisdukcapil', 'OK');
      } else {
        processVariables.set('statusDisdukcapil', 'Not OK');
      }

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleCheckImigration({ task, taskService }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const check: IImigrasiRequest = {
        identityNumber: task.variables.get('identityNumber'),
      };

      const status = await this.regulationService.checkImigration(check);
      if (status.allowedToFly) {
        processVariables.set('statusImigration', 'OK');
      } else {
        processVariables.set('statusImigration', 'Not OK');
      }

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleCheckPayment({ task, taskService }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const paymentId = task.variables.get('paymentId');
      const status = await this.bookingService.checkPayment(paymentId);
      const checkTime = status.checkTime ? status.checkTime : null;
      const paymentTime = status.paymentTime ? status.paymentTime : null;
      processVariables.set('checkTime', checkTime);
      processVariables.set('paymentStatus', status.paymentStatus);
      processVariables.set('paymentTime', paymentTime);

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleCheckPeduliLindungi({ task, taskService }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const check: IPeduliLindungiRequest = {
        identityNumber: task.variables.get('identityNumber'),
      };

      const status = await this.regulationService.checkPeduliLindungi(check);
      if (status.vaksinCovid === 3) {
        processVariables.set('statusVaksin', 'OK');
        processVariables.set('shouldPCR', false);
      } else {
        processVariables.set('statusVaksin', 'Not OK');
        processVariables.set('shouldPCR', true);
      }

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleSendMailBookingCode({ task, taskService }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const bookingCode = Math.random().toString(36).toUpperCase().slice(2, 10);
      processVariables.set('bookingCode', bookingCode);

      const timezone = task.variables.get('timezone');
      const reservationTimeStr = task.variables.get('reservationTime');
      const deadlineTime = calculateDeadlineBooking(
        reservationTimeStr,
        timezone,
      );

      const dataMail: IMailNormal = {
        mailTo: task.variables.get('email'),
        flightCode: task.variables.get('flightCode'),
        flightDate: task.variables.get('flightDate'),
        departureAirport: task.variables.get('departureAirport'),
        departureCode: task.variables.get('departureCode'),
        departureTime: task.variables.get('departureTime'),
        departureTimezone: task.variables.get('departureTimezone'),
        destinationAirport: task.variables.get('destinationAirport'),
        destinationCode: task.variables.get('destinationCode'),
        arrivalTime: task.variables.get('arrivalTime'),
        arrivalTimezone: task.variables.get('arrivalTimezone'),
        seatNumber: task.variables.get('seatNumber'),
        typeCode: 'Booking Code',
        realCode: bookingCode,
        nextProcess: 'pembayaran',
        deadline: deadlineTime,
        consequence: 'pembatalan kursi',
        specialNote: '-',
      };

      const dataReservation: IReservationUpdateRequest = {
        id: Number(task.businessKey),
        journeyTime: new Date().toISOString(),
        status: EReservationStatus.SUCCESSFUL_BOOKED,
        bookingCode,
      };

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.updateReservation(dataReservation);
      this.sendMailNormal(dataMail);
      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleSendMailReservationCode({
    task,
    taskService,
  }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const reservationCode = Math.random()
        .toString(36)
        .toUpperCase()
        .slice(2, 7);

      const ticketNumber = Math.random()
        .toFixed(13)
        .replace('0.', '')
        .padEnd(13, '2');

      processVariables.set('reservationCode', reservationCode);
      processVariables.set('ticketNumber', ticketNumber);

      const flightTimeStr = `${task.variables.get(
        'flightDate',
      )} ${task.variables.get('departureTime')}`;
      const departureTimezoneCode = task.variables.get('departureTimezone');
      const shouldPCR = task.variables.get('shouldPCR');

      const deadlineTime = calculateDeadlineRedeem(
        flightTimeStr,
        departureTimezoneCode,
      );

      const dataMail: IMailNormal = {
        mailTo: task.variables.get('email'),
        flightCode: task.variables.get('flightCode'),
        flightDate: task.variables.get('flightDate'),
        departureAirport: task.variables.get('departureAirport'),
        departureCode: task.variables.get('departureCode'),
        departureTime: task.variables.get('departureTime'),
        departureTimezone: task.variables.get('departureTimezone'),
        destinationAirport: task.variables.get('destinationAirport'),
        destinationCode: task.variables.get('destinationCode'),
        arrivalTime: task.variables.get('arrivalTime'),
        arrivalTimezone: task.variables.get('arrivalTimezone'),
        seatNumber: task.variables.get('seatNumber'),
        typeCode: 'Reservation Code/ETicket Number',
        realCode: `${reservationCode} / ${ticketNumber}`,
        nextProcess: 'redeem tiket',
        deadline: `${deadlineTime} ${task.variables.get('departureTimezone')}`,
        consequence: 'tiket hangus',
        specialNote: shouldPCR ? 'lampirkan hasil test PCR' : '-',
      };

      const dataReservation: IReservationUpdateRequest = {
        id: Number(task.businessKey),
        status: EReservationStatus.PAID,
        journeyTime: new Date().toISOString(),
        reservationCode,
        ticketNumber,
      };

      const seatStatusData: ISeatStatusUpdate = {
        flightDate: task.variables.get('flightDate'),
        flight: task.variables.get('flight'),
        seat: task.variables.get('seat'),
        seatStatus: ESeatStatus.PAID,
      };

      this.bookingClient.emit(
        EPatternMessage.UPDATE_SEAT_STATUS,
        seatStatusData,
      );
      this.logger.log(`Emit data update seat status done`);

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.updateReservation(dataReservation);
      this.sendMailNormal(dataMail);
      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleSendMailNotPassedReservation({
    task,
    taskService,
  }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const statusMember = task.variables.get('statusMember');
      const statusDisdukcapil = task.variables.get('statusDisdukcapil');
      const statusImigration = task.variables.get('statusImigration');
      const reasons: string[] = [];
      if (statusMember !== 'OK') {
        reasons.push('failed check member');
      }

      if (statusImigration !== 'OK') {
        reasons.push('failed check imigration');
      }

      if (statusDisdukcapil !== 'OK') {
        reasons.push('failed check disdukcapil');
      }

      const reason = reasons.join(',');

      const dataMail: IMailDecline = {
        mailTo: task.variables.get('email'),
        flightCode: task.variables.get('flightCode'),
        flightDate: task.variables.get('flightDate'),
        departureAirport: task.variables.get('departureAirport'),
        departureCode: task.variables.get('departureCode'),
        departureTime: task.variables.get('departureTime'),
        departureTimezone: task.variables.get('departureTimezone'),
        destinationAirport: task.variables.get('destinationAirport'),
        destinationCode: task.variables.get('destinationCode'),
        arrivalTime: task.variables.get('arrivalTime'),
        arrivalTimezone: task.variables.get('arrivalTimezone'),
        seatNumber: task.variables.get('seatNumber'),
        reservationStatus: 'DIBATALKAN',
        reason,
      };

      const dataReservation: IReservationUpdateRequest = {
        id: Number(task.businessKey),
        status: EReservationStatus.REGULATION_FAILED,
        journeyTime: new Date().toISOString(),
      };

      const seatStatusData: ISeatStatusUpdate = {
        flightDate: task.variables.get('flightDate'),
        flight: task.variables.get('flight'),
        seat: task.variables.get('seat'),
        seatStatus: ESeatStatus.UNLOCKED,
      };

      this.bookingClient.emit(
        EPatternMessage.UPDATE_SEAT_STATUS,
        seatStatusData,
      );
      this.logger.log(`Emit data update seat status done`);

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.updateReservation(dataReservation);
      this.sendMailDecline(dataMail);
      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleSendMailUnsuccessfulPayment({
    task,
    taskService,
  }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();

      const dataMail: IMailDecline = {
        mailTo: task.variables.get('email'),
        flightCode: task.variables.get('flightCode'),
        flightDate: task.variables.get('flightDate'),
        departureAirport: task.variables.get('departureAirport'),
        departureCode: task.variables.get('departureCode'),
        departureTime: task.variables.get('departureTime'),
        departureTimezone: task.variables.get('departureTimezone'),
        destinationAirport: task.variables.get('destinationAirport'),
        destinationCode: task.variables.get('destinationCode'),
        arrivalTime: task.variables.get('arrivalTime'),
        arrivalTimezone: task.variables.get('arrivalTimezone'),
        seatNumber: task.variables.get('seatNumber'),
        reservationStatus: 'DIBATALKAN',
        reason: 'gagal pembayaran',
      };

      const dataReservation: IReservationUpdateRequest = {
        id: Number(task.businessKey),
        status: EReservationStatus.UNSUCCESSFUL_PAYMENT,
        journeyTime: new Date().toISOString(),
      };

      const seatStatusData: ISeatStatusUpdate = {
        flightDate: task.variables.get('flightDate'),
        flight: task.variables.get('flight'),
        seat: task.variables.get('seat'),
        seatStatus: ESeatStatus.UNPAID,
      };

      this.bookingClient.emit(
        EPatternMessage.UPDATE_SEAT_STATUS,
        seatStatusData,
      );
      this.logger.log(`Emit data update seat status done`);

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.updateReservation(dataReservation);
      this.sendMailDecline(dataMail);
      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleSendMailUnsuccessfulRedeemTicket({
    task,
    taskService,
  }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();

      const dataMail: IMailDecline = {
        mailTo: task.variables.get('email'),
        flightCode: task.variables.get('flightCode'),
        flightDate: task.variables.get('flightDate'),
        departureAirport: task.variables.get('departureAirport'),
        departureCode: task.variables.get('departureCode'),
        departureTime: task.variables.get('departureTime'),
        departureTimezone: task.variables.get('departureTimezone'),
        destinationAirport: task.variables.get('destinationAirport'),
        destinationCode: task.variables.get('destinationCode'),
        arrivalTime: task.variables.get('arrivalTime'),
        arrivalTimezone: task.variables.get('arrivalTimezone'),
        seatNumber: task.variables.get('seatNumber'),
        reservationStatus: 'TIKET HANGUS',
        reason: 'gagal redeem tiket',
      };

      const dataReservation: IReservationUpdateRequest = {
        id: Number(task.businessKey),
        status: EReservationStatus.UNREDEEMED,
        journeyTime: new Date().toISOString(),
      };

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.updateReservation(dataReservation);
      this.sendMailDecline(dataMail);
      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleCalculateDiscountDestination({
    task,
    taskService,
  }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const check: IDestinationRequest = {
        id: this.configService.get<string>('ID_DESTINATION'),
        destination: task.variables.get('destination'),
      };

      const status = await this.ruleService.getDestination(check);
      processVariables.set('discountDestination', status.discountDestination);

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleCalculateDiscountFastPayment({
    task,
    taskService,
  }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();

      const reservationTime = task.variables.get('reservationTime');
      const commitPaymentTime = task.variables.get('commitPaymentTime');

      const msInMinutes = 1000 * 60;
      const startTime = new Date(reservationTime).getTime();
      const endTime = new Date(commitPaymentTime).getTime();
      const durationInMinutes =
        Math.round((endTime - startTime) / msInMinutes) === 0
          ? 1
          : Math.round((endTime - startTime) / msInMinutes);

      const check: IFastPaymentRequest = {
        id: this.configService.get<string>('ID_FASTPAYMENT'),
        minutesDurationFromBooking: durationInMinutes,
      };

      const status = await this.ruleService.getFastPayment(check);
      processVariables.set('discountFastPayment', status.discountFastPayment);

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }

  async handleSendMailUnpaidReservation({
    task,
    taskService,
  }: camunda.HandlerArgs) {
    try {
      const processVariables = new Variables();
      const dataMail: IMailDecline = {
        mailTo: task.variables.get('email'),
        flightCode: task.variables.get('flightCode'),
        flightDate: task.variables.get('flightDate'),
        departureAirport: task.variables.get('departureAirport'),
        departureCode: task.variables.get('departureCode'),
        departureTime: task.variables.get('departureTime'),
        departureTimezone: task.variables.get('departureTimezone'),
        destinationAirport: task.variables.get('destinationAirport'),
        destinationCode: task.variables.get('destinationCode'),
        arrivalTime: task.variables.get('arrivalTime'),
        arrivalTimezone: task.variables.get('arrivalTimezone'),
        seatNumber: task.variables.get('seatNumber'),
        reservationStatus: 'DIBATALKAN',
        reason: 'tidak ada pembayaran',
      };

      const dataReservation: IReservationUpdateRequest = {
        id: Number(task.businessKey),
        status: EReservationStatus.UNPAID,
        journeyTime: new Date().toISOString(),
      };

      const seatStatusData: ISeatStatusUpdate = {
        flightDate: task.variables.get('flightDate'),
        flight: task.variables.get('flight'),
        seat: task.variables.get('seat'),
        seatStatus: ESeatStatus.UNPAID,
      };

      this.bookingClient.emit(
        EPatternMessage.UPDATE_SEAT_STATUS,
        seatStatusData,
      );
      this.logger.log(`Emit data update seat status done`);

      await taskService.complete(task, processVariables);
      this.logger.log(`${task.topicName} ${task.id} completed`);

      this.updateReservation(dataReservation);
      this.sendMailDecline(dataMail);
      this.reportWorkflow(task);
    } catch (e) {
      this.logger.error(`Failed complete task ${task.id}, ${e}`);
    }
  }
}
