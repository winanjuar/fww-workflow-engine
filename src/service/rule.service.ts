import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import {
  IAxiosBaseError,
  IAxiosError,
} from 'src/interface/axios-error.interface';
import { IDestinationResponse } from 'src/interface/destination-response.interface';
import { IDestinationRequest } from 'src/interface/destination-request.interface';
import { IFastPaymentRequest } from 'src/interface/fast-payment-request.interface';
import { IFastPaymentResponse } from 'src/interface/fast-payment-response.interface';

@Injectable()
export class RuleService {
  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  private readonly logger = new Logger(RuleService.name);
  readonly baseUrl = `http://${this.configService.get<string>(
    'RULE_HOST',
  )}:${this.configService.get<string>('RULE_PORT')}/api`;
  private readonly auth = {
    username: this.configService.get<string>('RULE_USER'),
    password: this.configService.get<string>('RULE_PASS'),
  };

  async getDestination(check: IDestinationRequest) {
    const url = `${this.baseUrl}/v1/destination`;

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.post(url, check, { auth: this.auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch result rule destination');
    return data as IDestinationResponse;
  }

  async getFastPayment(check: IFastPaymentRequest) {
    const url = `${this.baseUrl}/v1/fast-payment`;

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.post(url, check, { auth: this.auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch result rule fast payment');
    return data as IFastPaymentResponse;
  }
}
