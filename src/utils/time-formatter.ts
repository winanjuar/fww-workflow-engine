import { ETimezoneCode } from 'src/core/timezone-code.enum';
import { ETimezone } from 'src/core/timezone.enum';

export const calculateDeadlineBooking = (
  bookingTime: string,
  timezone: ETimezone,
) => {
  const bookingTimeUTC = new Date(bookingTime);

  let adjustedWithTimezone: Date;
  if (timezone === ETimezone.WIB) {
    adjustedWithTimezone = new Date(
      bookingTimeUTC.getTime() - -420 * 60 * 1000,
    );
  } else if (timezone === ETimezone.WITA) {
    adjustedWithTimezone = new Date(
      bookingTimeUTC.getTime() - -480 * 60 * 1000,
    );
  } else {
    adjustedWithTimezone = new Date(
      bookingTimeUTC.getTime() - -540 * 60 * 1000,
    );
  }

  const deadlineTime = new Date(
    adjustedWithTimezone.getTime() + 6 * 60 * 60 * 1000,
  );

  return deadlineTime.toISOString().slice(0, 19).replace('T', ' ');
};

export const calculateDeadlineRedeem = (
  flightTime: string,
  timezone: ETimezoneCode,
) => {
  const flightTimeUTC = new Date(flightTime);

  let adjustedWithTimezone: Date;
  if (timezone === ETimezoneCode.WIB) {
    adjustedWithTimezone = new Date(flightTimeUTC.getTime() - -420 * 60 * 1000);
  } else if (timezone === ETimezoneCode.WITA) {
    adjustedWithTimezone = new Date(flightTimeUTC.getTime() - -480 * 60 * 1000);
  } else {
    adjustedWithTimezone = new Date(flightTimeUTC.getTime() - -540 * 60 * 1000);
  }

  const deadlineTime = new Date(
    adjustedWithTimezone.getTime() - 1 * 60 * 60 * 1000,
  );

  return deadlineTime.toISOString().slice(0, 19).replace('T', ' ');
};
