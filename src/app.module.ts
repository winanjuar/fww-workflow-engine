import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { HttpModule } from '@nestjs/axios';

import { AppService } from './service/app.service';
import { MemberService } from './service/member.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { RegulationService } from './service/regulation.service';
import { RuleService } from './service/rule.service';
import { BookingService } from './service/booking.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    LoggerModule.forRoot({
      pinoHttp: {
        formatters: {
          level: (label: string) => {
            return { level: label.toUpperCase() };
          },
        },
        customLevels: {
          emergerncy: 80,
          alert: 70,
          critical: 60,
          error: 50,
          warn: 40,
          notice: 30,
          info: 20,
          debug: 10,
        },
        useOnlyCustomLevels: true,
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
            colorize: true,
            levelFirst: true,
            translateTime: 'SYS:standard',
            ignore: 'hostname,pid',
          },
        },
      },
    }),
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5,
      }),
    }),
    ClientsModule.registerAsync([
      {
        name: 'BookingEngine',
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [configService.get<string>('RMQ_URL')],
            queue: configService.get<string>('RMQ_BOOKING_QUEUE'),
            queueOptions: { durable: false },
            prefetchCount: 1,
          },
        }),
      },
      {
        name: 'CoreEngine',
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [configService.get<string>('RMQ_URL')],
            queue: configService.get<string>('RMQ_CORE_QUEUE'),
            queueOptions: { durable: false },
            prefetchCount: 1,
          },
        }),
      },
    ]),
  ],
  controllers: [],
  providers: [
    AppService,
    MemberService,
    RegulationService,
    RuleService,
    BookingService,
  ],
})
export class AppModule {}
