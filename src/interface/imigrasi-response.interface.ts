export interface IImigrasiResponse {
  identityNumber: string;
  allowedToFly: boolean;
  reason: string;
}
