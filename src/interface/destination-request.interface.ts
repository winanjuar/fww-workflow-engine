export interface IDestinationRequest {
  id: string;
  destination: string;
}
