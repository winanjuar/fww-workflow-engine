export interface IDisdukcapilResponse {
  identityNumber: string;
  match: boolean;
}
