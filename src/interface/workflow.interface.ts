export interface IWorkflowMaster {
  processInstanceId: string;
  reservationId?: number;
  details?: IWorkflowDetail[];
}

export interface IWorkflowDetail {
  workflow?: IWorkflowMaster;
  taskId: string;
  activityName: string;
}
