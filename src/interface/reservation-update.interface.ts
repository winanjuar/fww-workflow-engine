import { EReservationStatus } from 'src/core/reservation-status.enum';

export interface IReservationUpdateRequest {
  id: number;
  status: EReservationStatus;
  journeyTime: string;
  bookingCode?: string;
  reservationCode?: string;
  ticketNumber?: string;
}

export interface IReservationUpdateData {
  currentStatus: EReservationStatus;
  bookingCode?: string;
  reservationCode?: string;
  ticketNumber?: string;
}
