export interface IMemberResponse {
  id: number;
  isBlacklist: boolean;
}
