export interface IFastPaymentRequest {
  id: string;
  minutesDurationFromBooking: number;
}
