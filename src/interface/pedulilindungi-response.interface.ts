export interface IPeduliLindungiResponse {
  identityNumber: string;
  vaksinCovid: number;
  shouldPCR: boolean;
}
