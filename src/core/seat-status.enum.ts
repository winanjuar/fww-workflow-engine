export enum ESeatStatus {
  SELECTED = 'SELECTED', // dipilih calon passenger
  RELEASED = 'RELEASED', //gak jadi dipilih calon passenger
  BOOKED = 'BOOKED', //sudah dibooking, masih menunggu dibayar
  PAID = 'PAID', //sudah dibayar
  UNLOCKED = 'UNLOCKED', //sudah diselect, tp passenger gagal pengecekan regulasi
  RESET = 'RESET', //klo statusnya selected terus tapi gak jadi booking
  UNPAID = 'UNPAID', //klo statusnya booked tapi setelah 6 jam tidak ada pembayaran
}
